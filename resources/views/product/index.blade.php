@extends('home')
@section('content')

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Create Product
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/product" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product-name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="product-name">
                        </div>
                        <div class="form-group">
                            <label for="product-description" class="col-form-label">Description:</label>
                            <input type="text" name="description" class="form-control" id="product-description">
                        </div>
                        <div class="form-group">
                            <label for="product-price" class="col-form-label">Price:</label>
                            <input type="number" name="price" class="form-control" id="product-price">
                        </div>
                        <div class="form-group">
                            <label for="product-images" class="col-form-label">Images:</label>
                            <input type="file" class="form-control" name="images[]" id="product-images" multiple />
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="category_id" id="category_id">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" @selected($selectedCategory == $category->id)>
                                        {{ $category->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div>
        <form action="/product" method="get">
            <div class="input-group">
                <div class="form-outline d-inline">
                    <input type="search" name="name" id="form1" class="form-control" />
                    <label class="form-label" for="form1">Name</label>
                </div>
            </div>
            <div class="input-group">
                <div class="form-outline  d-inline">
                    <select class="form-control" name="category_id" id="category_id">
                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" selected='{{$selectedCategory == $category->id}}'>
                                {{ $category->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="input-group">
                <div class="form-outline d-inline">
                    <label for="product-price-min" class="col-form-label">Price Min:</label>
                    <input type="number" name="price_min" class="form-control" id="product-price-min">
                </div>
            </div>
            <div class="input-group">
                <div class="form-outline d-inline">
                    <label for="product-price-max" class="col-form-label">Price Max:</label>
                    <input type="number" name="price_max" class="form-control" id="product-price-max">
                </div>
            </div>
            <button type="submit" class="btn btn-outline-primary">Search</button>

        </form>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col">Price</th>
            <th scope="col">Images</th>
        </tr>
        </thead>
        <tbody>
        @foreach($products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>
                    {{$product->name}}
                </td>
                <td>
                    {{$product->description}}
                </td>
                <td>
                    {{$product->price}}
                </td>
                <td>
                    @foreach($product->images as $image)
                        <img src="{{url('storage/' . $image->path)}}">
                    @endforeach
                </td>
                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalProduct-{{$product->id}}">
                        Edit Product
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalProduct-{{$product->id}}" tabindex="-1" aria-labelledby="exampleModalProductLabel-{{$product->id}}" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="{{route('product.update', ['product' => $product])}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <input type="hidden" id="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="product-name" class="col-form-label">Name:</label>
                                            <input type="text" name="name" class="form-control" id="product-{{$product->id}}-name" value="{{$product->name}}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>

                <td>
                    <div>
                        <form action="{{route('product.destroy', ['product' => $product])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary">
                                Delete Product
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
