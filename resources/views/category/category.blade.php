@extends('home')
@section('content')

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Create Product
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/product" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="product-name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="product-name">
                        </div>
                        <div class="form-group">
                            <label for="product-description" class="col-form-label">Description:</label>
                            <input type="text" name="description" class="form-control" id="product-description">
                        </div>
                        <div class="form-group">
                            <label for="product-price" class="col-form-label">Price:</label>
                            <input type="number" name="price" class="form-control" id="product-price">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category->products as $product)
            <tr>
                <th scope="row">{{$product->id}}</th>
                <td>
                    <a href="{{route('product.show', ['product' => $product])}}">
                        {{$product->name}}
                    </a>
                </td>
                <td>
                    <div>
                        <form action="{{route('product.destroy', ['product' => $product])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary">
                                Delete Product
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
