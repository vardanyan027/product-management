@extends('home')
@section('content')

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Create Category
    </button>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="/category" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="category-name" class="col-form-label">Name:</label>
                            <input type="text" name="name" class="form-control" id="category-name">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
        </tr>
        </thead>
        <tbody>
        @foreach($categories as $category)
            <tr>
                <th scope="row">{{$category->id}}</th>
                <td>
                    <a href="{{route('product.index', ['category_id' => $category->id])}}">
                        {{$category->name}}
                    </a>
                </td>

                <td>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCategory-{{$category->id}}">
                        Edit Category
                    </button>

                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalCategory-{{$category->id}}" tabindex="-1" aria-labelledby="exampleModalCategoryLabel-{{$category->id}}" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <form action="{{route('category.update', ['category' => $category])}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="modal-body">
                                        <input type="hidden" id="_token" value="{{csrf_token()}}">
                                        <div class="form-group">
                                            <label for="category-name" class="col-form-label">Name:</label>
                                            <input type="text" name="name" class="form-control" id="category-{{$category->id}}-name" value="{{$category->name}}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>

                <td>
                    <div>
                        <form action="{{route('category.destroy', ['category' => $category])}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-primary">
                                Delete Category
                            </button>
                        </form>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

@endsection
