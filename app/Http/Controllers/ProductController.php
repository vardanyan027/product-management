<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrEditProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(Request $request)
    {
        $product = Product::query();

        $selectedCategory = '';

        if ($request->get('category_id')) {
            $category_id = $request->get('category_id');
            $product->where('category_id', $category_id);
            $selectedCategory = $category_id;
        }

        if ($request->get('name')) {
            $name = $request->get('name');
            $product->where('name', 'like', '%' .$name . '%');
        }

        if ($request->get('price_min')) {
            $price_min = $request->get('price_min');
            $product->where('price', '=<', $price_min);
        }

        if ($request->get('price_max')) {
            $price_max = $request->get('price_max');
            $product->where('price', '>=', $price_max);
        }

        return view('product.index', [
            'products' => $product->get(),
            'categories' => Category::all(),
            'selectedCategory' => $selectedCategory
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrEditProductRequest $request
     * @return RedirectResponse
     */
    public function store(CreateOrEditProductRequest $request): RedirectResponse
    {
        try {
            $product = new Product();
            $this->createOrUpdate($request, $product);
            return back();
        } catch (\Exception $exception) {
            return back()->withErrors(['errors' => $exception->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateOrEditProductRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(CreateOrEditProductRequest $request, int $id): RedirectResponse
    {
        try {
            $product = Product::where('id', $id)->first();
            $this->createOrUpdate($request, $product);
            return back();
        } catch (\Exception $exception) {
            return back()->withErrors(['errors' => $exception->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse|void
     */
    public function destroy(int $id)
    {
        try {
            $product = Product::where('id', $id)->first();
            $product->images()->delete();
            $product->delete();
            return back();
        } catch (\Exception $exception) {
            return back()->withErrors(['errors' => $exception->getMessage()]);
        }
    }

    public function createOrUpdate(CreateOrEditProductRequest $request, Product $product) {
        DB::transaction(function () use ($request, $product) {
            $product->name = $request->get('name');
            $product->description = $request->get('description');
            $product->price = $request->get('price');
            $product->category_id = $request->get('category_id');

            $product->save();

            $product->images()->delete();
            $files = $request->allFiles();
            foreach ($files as $file) {
                Storage::disk('local')->put('public/' . $file[0]->getClientOriginalName(), file_get_contents($file[0]));
                $product->images()->create([
                    'path' => $file[0]->getClientOriginalName()
                ]);
            }

            if (!isset($product->id)) {
                throw new \Exception('Product not created');
            }

            if (!isset($product->images()->first()->id)) {
                throw new \Exception('Image not created');
            }
        });
    }
}
