<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use http\Env\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{
    public function login(LoginRequest $loginRequest)
    {
        $email = $loginRequest->get('email');
        $password = $loginRequest->get('password');

        $user = User::where('email', $email)->first();

        if(!isset($user)) {
            return back()->withErrors([
                'message' => 'user not found'
            ]);
        }

        if(Hash::check($password, $user->password)) {
            Auth::attempt([
                'email' => $email,
                'password' => $password
            ]);
            return redirect('/category');
        }

        return back()->withErrors([
            'message' => 'credentials is incorrect'
        ]);
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
