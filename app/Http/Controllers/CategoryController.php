<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOrEditCategoryRequest;
use App\Models\Category;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = Category::all();

        return view('category.index', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateOrEditCategoryRequest $request
     * @return RedirectResponse
     */
    public function store(CreateOrEditCategoryRequest $request): RedirectResponse
    {
        $category = new Category();
        $category->name = $request->get('name');
        $category->save();

        return Redirect::route('category.show', ['category' => $category]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View
     */
    public function show(int $id)
    {
        $category = Category::where('id', $id)->first();

        return view('category.category', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateOrEditCategoryRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(CreateOrEditCategoryRequest $request, int $id): RedirectResponse
    {
        $category = Category::where('id', $id)->first();

        $category->name = $request->get('name');
        $category->save();

        return Redirect::route('category.show', ['category' => $category]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        try {
            Category::destroy($id);
            return Redirect::route('category.index');
        } catch (\Exception $exception) {
            return back()->withErrors([
                'message' => $exception->getMessage()
            ]);
        }
    }
}
